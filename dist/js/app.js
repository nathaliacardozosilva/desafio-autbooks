const produtos = document.querySelector('#produtos');
const listaLivros = [
    {
        id: 0,
        imagem: "assets/img/produto-1.png",
        nome: "Harry Potter e a Pedra Filosofal",
        valorInicial: 40,
        valorTotal: 0,
        quantidade: 0
    },
    {
        id: 1,
        imagem: "assets/img/produto-2.png",
        nome: "Harry Potter e a Câmara Secreta",
        valorInicial: 40,
        valorTotal: 0,
        quantidade: 0
    },
    {
        id: 2,
        imagem: "assets/img/produto-3.png",
        nome: "Harry Potter e o Prisioneiro de Azkaban",
        valorInicial: 40,
        valorTotal: 0,
        quantidade: 0
    },
    {
        id: 3,
        imagem: "assets/img/produto-4.png",
        nome: "Harry Potter e o Cálice de Fogo",
        valorInicial: 40,
        valorTotal: 0,
        quantidade: 0
    },
    {
        id: 4,
        imagem: "assets/img/produto-5.png",
        nome: "Harry Potter e a Ordem da Fênix",
        valorInicial: 40,
        valorTotal: 0,
        quantidade: 0
    },
    {
        id: 5,
        imagem: "assets/img/produto-6.png",
        nome: "Harry Potter e o Enigma do Príncipe",
        valorInicial: 40,
        valorTotal: 0,
        quantidade: 0
    },
    {
        id: 6,
        imagem: "assets/img/produto-7.png",
        nome: "Harry Potter e as Relíquias da Morte",
        valorInicial: 40,
        valorTotal: 0,
        quantidade: 0
    },
    {
        id: 7,
        imagem: "assets/img/produto-8.png",
        nome: "Hunger Games - Volume 1",
        valorInicial: 40,
        valorTotal: 0,
        quantidade: 0
    }
];
function preencherLivros() {
    for (var i = 0; i < listaLivros.length; i++) {
        produtos.innerHTML += "<article class='produtos__produto'><figure class='produto__figura'><img src='" + listaLivros[i].imagem + "' alt='" + listaLivros[i].nome + "' class='produto__imagem'><figcaption class='produto__titulo'>" + listaLivros[i].nome + "</figcaption></figure><p class='produto__preco'>R$ " + listaLivros[i].valorInicial + ",00</p>" + "<button id='" + i + "' class='produto__botao' onclick='adicionarLivro(" + i + "), snackbar()'>Adicionar ao carrinho</button>" + "</article>";
    }
}
preencherLivros();
let listaDeLivrosCarrinho = [];
function adicionarLivro(id) {
    listaLivros[id].quantidade = listaLivros[id].quantidade + 1;
    listaLivros[id].valorTotal = listaLivros[id].quantidade * listaLivros[id].valorInicial;
    let soma = 0;
    for (const item of listaLivros) {
        soma += item.valorTotal;
    }
    listaLivros.map((id) => {
        if (id.quantidade > 0) {
            listaDeLivrosCarrinho.push(id.id);
        }
    });
    const novaLista = [...new Set(listaDeLivrosCarrinho)];
    calcularDesconto(novaLista, soma);
    atualizarCarrinho();
}
function calcularDesconto(novaLista, soma) {
    let desconto;
    if (novaLista.length == 2) {
        desconto = (soma * 0.05);
    }
    else if (novaLista.length == 3) {
        desconto = soma * 0.1;
    }
    else if (novaLista.length == 4) {
        desconto = soma * 0.2;
    }
    else if (novaLista.length >= 5) {
        desconto = soma * 0.25;
    }
    else {
        desconto = 0;
    }
    const containerValor = document.querySelector(".carrinho__valores");
    containerValor.innerHTML = '';
    containerValor.innerHTML += `
        <p>R$ ${soma.toFixed(2)}</p>
        <p>R$ ${desconto.toFixed(2)}</p>
        <p class="carrinho__valores__total">R$ ${(soma - desconto).toFixed(2)}</p>
    `;
}
function atualizarCarrinho() {
    const containerCarrinho = document.querySelector('.carrinho__container');
    containerCarrinho.innerHTML = '';
    listaLivros.map((id) => {
        if (id.quantidade > 0) {
            containerCarrinho.innerHTML += `
                <article class="carrinho__produto">
                    <img src="${id.imagem}" alt="${id.nome}" class="carrinho__produto__imagem">
                    <div class="carrinho__produto__info">
                        <p class="carrinho__produto__titulo">${id.nome}</p>
                        <div class="carrinho__produto__valores">
                            <p class="carrinho__produto__preco">R$ ${id.valorTotal},00</p>
                            <p class="carrinho__produto__quantidade">${id.quantidade}</p>
                            <img src="./assets/img/icones/lixeira.svg" class="carrinho__produto__excluir">
                        </div>
                    </div>
                </article>
            `;
            listaDeLivrosCarrinho.push(id.id);
        }
    });
}
