function menuCascata() {
    const menu = document.querySelector('.menu');
    const menuLateral = document.querySelector('.menu-lateral');
    const fundo = document.querySelector('.fundo-escuro-menu-lateral');
    if (window.innerWidth > 1024) {
        menu.classList.toggle('menu--ativo');
    }
    else if (window.innerWidth <= 1024) {
        menuLateral.classList.toggle('menu-lateral--ativo');
        fundo.classList.toggle('fundo-escuro-menu-lateral--ativo');
    }
}
function carrinhoCascata() {
    const carrinho = document.querySelector('.carrinho');
    carrinho.classList.toggle('carrinho--ativo');
}
function modal() {
    const modal = document.querySelector('.regulamento');
    const fundo = document.querySelector('.fundo-escuro');
    modal.classList.toggle('regulamento--ativo');
    fundo.classList.toggle('fundo-escuro--ativo');
}
function snackbar() {
    const snackbar = document.querySelector('.snackbar');
    snackbar.classList.toggle('snackbar--ativo');
    function snackbarHide() {
        snackbar.classList.toggle('snackbar--ativo');
    }
    setTimeout(() => {
        snackbarHide();
    }, 2000);
}
